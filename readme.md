![bannière sophie](./public/assets/images/sophie-bluel.png)
# Site de Sophie Bluel - Bases de JavaScript

Dans ce projet le but était de manipuler le DOM afin d'apprendre et de développer les bases du langage JavaScript coté client. Nous avons ainsi chargé dynamiquement les images dans la galerie, créé le filtre de catégories de la galerie, puis ajouté une connexion utilisateur en enregistrant les données dans le localStorage. Enfin, nous avons terminé par ajouté une sommaire interface de gestion des médias, avec la suppression des images et l'ajout de nouvelles.

# Sommaire
+ [Site de Sophie Bluel - Bases de JavaScript](#site-de-sophie-bluel-bases-de-javascript)
+ [Sommaire](#sommaire)
+ [Installation et lancement](#installation-et-lancement)
+ [Démonstration](#démonstration)
+ [Technologies utilisées](#technologies-utilisées)
+ [License](#license)

# Installation et lancement

Voici les étapes à suivre pour lancer le projet en local :


Prérequis : Vous devez avoir nodejs et npm installés et fonctionnels, suivez ce lien pour installer les dernières versions stables : [installer Node.Js](https://nodejs.org/fr)

Voici les étapes à suivre pour lancer le projet en local :

1. créer le dossier qui contiendra le projet : `mkdir myNewProject`
2. cloner le projet dans ce dossier : `git clone https://github.com/JeremyDarbellay/projet_8_openclassrooms.git ./myNewProjet`
3. naviguer dans ce dossier : `cd myNewProject/Backend`
4. installer les modules associés : `npm install`
5. Lancez `npm start` pour lancer le back-end en local !
6. Ensuite, il vous suffit de prévisualiser le html, VisualStudioCode permet de la faire assez facilement.

# Démonstration

Vous retrouverez la démonstration du site à cette adresse : [site de sophie](https://projets-jeremy-darbellay.gitlab.io/sophie_bluel).
Dans le code, les fichiers de démonstrations sont dans le dossier "public", comme il n'y a pas de back-end sur l'hébergement, toutes les fonctionnalités de communcation ont été désactivées

# Technologies utilisées

Pour ce projet, je n'ai utilisé que les langages du web, html, css et javascript, aucun framework ni aucune librairie n'a été ajoutée.

# License

[MIT](https://gitlab.com/projets-jeremy-darbellay/sophie_bluel/-/blob/master/LICENSE))
